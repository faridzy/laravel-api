<?php

namespace App\Http\Controllers;

use Illuminate\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Book;

class BookController extends Controller
{
    public function index()
    {
        $buku = Book::paginate(10);

        if (!$buku) {
            return response()->json([
                'status'=>false,
                'message'=>'Data tidak valid!'
            ]);
        }
        return response()->json([
            'status'=>true,
            'data'=>$buku
        ]);
    }

    public function show($id)
    {
        $buku=Book::find($id);
        if(is_null($buku)){
            return response()->json([
                'status'=>false,
                'message'=>'Data tidak ada']);
        }else{
            return response()->json([
                'status'=>true,
                $buku
            ]);
        }

    }

    public function store(Request $request)
    {
       $buku = new Book;
       $buku->title = $request->input('title');
       $buku->price = $request->input('price');
       $buku->author = $request->input('author');
       $buku->editor = $request->input('editor');

        if ($buku->save()) {
            return response()->json([
                'status'=>true,
                'message'=>'Data berhasil disimpan',
            ]);
        }else{
            return response()->json([
                'status'=>false,
                'message'=>'Data gagal disimpan',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
       $buku = Book::find($id);
       $buku->title = $request->input('title');
       $buku->price = $request->input('price');
       $buku->author = $request->input('author');
       $buku->editor = $request->input('editor');

        if ($buku->save()) {
            return response()->json([
                'status'=>true,
                'message'=>'Data berhasil diupdate'
            ]);
        }else{
            return response()->json([
                'status'=>false,
                'message'=>'Data gagal diupdate',
            ]);
        }
    }

    public function destroy($id)
    {
       
       $buku = Book::find($id);
        if ($buku->delete()) {
            return response()->json([
                'status'=>true,
                'message'=>'Data berhasil didelete'
            ]);
        }else{
            return response()->json([
                'status'=>false,
                'message'=>'Data gagal didelete',
            ]);
        }
      
    }
}
