<?php

/**
 * @Author: ELL
 * @Date:   2017-11-29 00:32:28
 * @Last Modified by:   ELL
 * @Last Modified time: 2017-11-29 00:39:39
 */

namespace App\Http\Controllers;

use Illuminate\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
* 
*/
class CobaController extends Controller
{
	public function index(){
		$result=[
			'sp_msg'=>'Smartpay : Success',
			'sp_desc'=>'Activation status successfully get'
		];
		$data=[
			'user_name'=>'Tasya Rosmala',
			'user_smartpay_number'=>'06493879529',
			'user_is_registered'=>1,
		];
		return response()->json([
			'sp_status'=>900,
			'result'=>$result,
			'data'=>$data
		]);

	}	
}